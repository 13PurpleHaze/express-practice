const rating = [
    {
        id: 1,
        fio: "Александров Игнат Анатольевич",
        totalGames: 24534,
        wins: 9824,
        loses: 1222,
        winsPercent: "87%"
    },
    {
        id: 2,
        fio: "Шевченко Рафаил Михаилович",
        totalGames: 24534,
        wins: 9824,
        loses: 1222,
        winsPercent: "87%"
    },
    {
        id: 3,
        fio: "Мазайло Трофим Артемович",
        totalGames: 24534,
        wins: 9824,
        loses: 1222,
        winsPercent: "87%"
    },
    {
        id: 4,
        fio: "Логинов Остин Даниловичч",
        totalGames: 24534,
        wins: 9824,
        loses: 1222,
        winsPercent: "87%"
    },
    {
        id: 5,
        fio: "Борисов Йошка Васильевич",
        totalGames: 24534,
        wins: 9824,
        loses: 1222,
        winsPercent: "87%"
    },
    {
        id: 6,
        fio: "Соловьев Ждан Михаилович",
        totalGames: 24534,
        wins: 9824,
        loses: 1222,
        winsPercent: "87%"
    },
    {
        id: 7,
        fio: "Негода Михаил Эдуардович",
        totalGames: 24534,
        wins: 9824,
        loses: 1222,
        winsPercent: "87%"
    },
    {
        id: 8,
        fio: "Гордеев Шамиль Леонидович",
        totalGames: 24534,
        wins: 9824,
        loses: 1222,
        winsPercent: "87%"
    },
    {
        id: 9,
        fio: "Многогрешный Павел Витальевич",
        totalGames: 24534,
        wins: 9824,
        loses: 1222,
        winsPercent: "87%"
    },
    {
        id: 10,
        fio: "Александров Игнат Анатольевич",
        totalGames: 24534,
        wins: 9824,
        loses: 1222,
        winsPercent: "87%"
    },
    {
        id: 11,
        fio: "Волков Эрик Алексеевич",
        totalGames: 24534,
        wins: 9824,
        loses: 1222,
        winsPercent: "87%"
    },
    {
        id: 12,
        fio: "Кузьмин Ростислав Васильевич",
        totalGames: 24534,
        wins: 9824,
        loses: 1222,
        winsPercent: "87%"
    },
    {
        id: 13,
        fio: "Стрелков Филипп Борисов",
        totalGames: 24534,
        wins: 9824,
        loses: 1222,
        winsPercent: "87%"
    },
    {
        id: 14,
        fio: "Галкин Феликс Платонович",
        totalGames: 24534,
        wins: 9824,
        loses: 1222,
        winsPercent: "87%"
    },
];
export const getRating = (req, res) => {
    res.render('rating', {rating});
}