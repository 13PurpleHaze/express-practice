import CryptoJS from 'crypto-js';
import fs from 'fs';
import path from "path";

export const auth = (req, res) => {
    const {login, password} = req.body;
    const __dirname = path.resolve();
    const file = path.join(__dirname, 'data', 'users.json');

    fs.readFile(file, 'utf8', (err, data) => {
        if (err) {
            console.error(err);
            return res.status(500).send('Ошибка сервера');
        }
        try {
            const users = JSON.parse(data);
            const user = users.find((u) => u.login === login);
            if (!user) {
                return res.status(401).send('Неправильный пароль или логин');
            }
            const decryptedPassword = CryptoJS.AES.decrypt(user.password, 'secret-key').toString(CryptoJS.enc.Utf8);

            if (decryptedPassword !== password) {
                return res.status(401).send('Неправильный пароль или логин');
            }
            res.redirect("/rating");
        } catch (err) {
            console.error(err);
            return res.status(500).send('Ошибка сервера');
        }
    });
}

