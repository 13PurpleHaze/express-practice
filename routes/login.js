import {Router} from "express";
import {auth} from "../controllers/loginController.js";

const router = new Router();

router.get("/login", (req, res) => {
    res.render('login');
});

router.post("/login", auth);
export default router;