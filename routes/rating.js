import {Router} from "express";
import {getRating} from "../controllers/ratingController.js"

const router = new Router();

router.get("/rating", getRating);

export default router;