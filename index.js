import express from "express";
import login from "./routes/login.js";
import rating from "./routes/rating.js";
import bodyParser from "body-parser";
import path from "path";

const app = express()

const __dirname = path.resolve();
app.use(express.static(__dirname + '/public'));
app.set('view engine', "ejs")

app.use(bodyParser.urlencoded({ extended: true }));
app.use(login)
app.use(rating)

const PORT = 8080;
app.listen(PORT);